package main

import (
	"os"

	"gitlab.com/prosign/server"
)

func main() {
	server.NewServer(os.Getenv("PROSIGN_DEBUG") == "true").Listen(":3000")
}
